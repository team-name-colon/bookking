**BookKing**

[![pipeline status](https://gitlab.com/SiddeeqLaher/bookking/badges/master/pipeline.svg)](https://gitlab.com/SiddeeqLaher/bookking/commits/master)

[![coverage report](https://gitlab.com/SiddeeqLaher/bookking/badges/master/coverage.svg)](https://gitlab.com/SiddeeqLaher/bookking/commits/master)

The only Moodle activity plugin you will ever need for booking time slots!
